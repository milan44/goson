package goson

import (
	"fmt"
	"testing"
)

func TestLoadJSONMap(t *testing.T) {
	res, err := LoadJObjectStrict("example.json")
	must(err)

	o, err := res.GetObjectStrict("a")
	must(err)
	fmt.Println("a", o)

	o, err = o.GetObjectStrict("b")
	must(err)
	fmt.Println("a.b", o)

	o2, err := o.GetSliceStrict("c")
	must(err)
	fmt.Println("a.b.c", o2, len(o2.Slice))

	o, err = o2.GetObjectStrict(0)
	must(err)
	fmt.Println("a.b.c.0", o)

	v, err := o.GetStrict("d", "")
	must(err)
	fmt.Println("a.b.c.0.d", v)

	v = res.GetObject("a").GetObject("b").GetSlice("c").GetObject(0).Get("d", "")
	fmt.Println("direct - a.b.c.0.d", v)
}

func must(err error) {
	if err != nil {
		panic(err)
	}
}
