package goson

import (
	"errors"
	"fmt"
	"reflect"
)

type Slice []interface{}

type JSlice struct {
	Slice
}

func (j JSlice) GetObjectStrict(index int) (*JObject, error) {
	if !(index < 0 || index >= len(j.Slice) || j.Slice == nil) {
		res := j.Slice[index]

		mp, ok := res.(map[string]interface{})

		if ok {
			return &JObject{mp}, nil
		}

		return nil, errors.New(fmt.Sprintf("index %d is not an object", index))
	}

	return nil, errors.New(fmt.Sprintf("index %d not set", index))
}

func (j JSlice) GetSliceStrict(index int) (*JSlice, error) {
	if !(index < 0 || index >= len(j.Slice) || j.Slice == nil) {
		res := j.Slice[index]

		mp, ok := res.([]interface{})

		if ok {
			return &JSlice{mp}, nil
		}

		return nil, errors.New(fmt.Sprintf("index %d is not a slice", index))
	}

	return nil, errors.New(fmt.Sprintf("index %d not set", index))
}

func (j JSlice) GetObject(index int) JObject {
	if !(index < 0 || index >= len(j.Slice) || j.Slice == nil) {
		res := j.Slice[index]

		mp, ok := res.(map[string]interface{})

		if ok {
			return JObject{mp}
		}

		return JObject{make(map[string]interface{})}
	}

	return JObject{make(map[string]interface{})}
}

func (j JSlice) GetSlice(index int) JSlice {
	if !(index < 0 || index >= len(j.Slice) || j.Slice == nil) {
		res := j.Slice[index]

		mp, ok := res.([]interface{})

		if ok {
			return JSlice{mp}
		}

		return JSlice{make([]interface{}, 0)}
	}

	return JSlice{make([]interface{}, 0)}
}

func (j JSlice) Get(index int, target interface{}) interface{} {
	typ := reflect.TypeOf(target)
	empty := reflect.New(typ)

	if !(index < 0 || index >= len(j.Slice) || j.Slice == nil) {
		res := j.Slice[index]

		if reflect.TypeOf(res).String() == typ.String() {
			return res
		}

		return empty
	}

	return empty
}

func (j JSlice) GetStrict(index int, target interface{}) (interface{}, error) {
	typ := reflect.TypeOf(target)

	if !(index < 0 || index >= len(j.Slice) || j.Slice == nil) {
		res := j.Slice[index]

		if reflect.TypeOf(res).String() == typ.String() {
			return res, nil
		}

		return nil, errors.New(fmt.Sprintf("index %d is not %s", index, typ.String()))
	}

	return nil, errors.New(fmt.Sprintf("index %d not set", index))
}
