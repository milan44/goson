package goson

import (
	"github.com/pkg/errors"
	"reflect"
)

type Object map[string]interface{}

type JObject struct {
	Object
}

func (j JObject) GetObjectStrict(key string) (*JObject, error) {
	res, ok := j.Object[key]
	if ok {
		mp, ok := res.(map[string]interface{})

		if ok {
			return &JObject{mp}, nil
		}

		return nil, errors.New("'" + key + "' is not an object")
	}

	return nil, errors.New("key '" + key + "' not set")
}

func (j JObject) GetSliceStrict(key string) (*JSlice, error) {
	res, ok := j.Object[key]
	if ok {
		mp, ok := res.([]interface{})

		if ok {
			return &JSlice{mp}, nil
		}

		return nil, errors.New("'" + key + "' is not a slice")
	}

	return nil, errors.New("key '" + key + "' not set")
}

func (j JObject) GetObject(key string) JObject {
	res, ok := j.Object[key]
	if ok {
		mp, ok := res.(map[string]interface{})

		if ok {
			return JObject{mp}
		}

		return JObject{make(map[string]interface{})}
	}

	return JObject{make(map[string]interface{})}
}

func (j JObject) GetSlice(key string) JSlice {
	res, ok := j.Object[key]
	if ok {
		mp, ok := res.([]interface{})

		if ok {
			return JSlice{mp}
		}

		return JSlice{make([]interface{}, 0)}
	}

	return JSlice{make([]interface{}, 0)}
}

func (j JObject) Get(key string, target interface{}) interface{} {
	typ := reflect.TypeOf(target)
	empty := reflect.New(typ)

	res, ok := j.Object[key]
	if ok {
		if reflect.TypeOf(res).String() == typ.String() {
			return res
		}

		return empty
	}

	return empty
}

func (j JObject) GetStrict(key string, target interface{}) (interface{}, error) {
	typ := reflect.TypeOf(target)
	empty := reflect.New(typ)

	res, ok := j.Object[key]
	if ok {
		if reflect.TypeOf(res).String() == typ.String() {
			return res, nil
		}

		return empty, errors.New("'" + key + "' is not " + typ.String())
	}

	return empty, errors.New("'" + key + "' is not set")
}
