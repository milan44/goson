package goson

import (
	"encoding/json"
	"github.com/pkg/errors"
	"io/ioutil"
	"os"
)

func LoadJObjectStrict(file string) (*JObject, error) {
	if _, err := os.Stat(file); err == nil {
		b, err := ioutil.ReadFile(file)
		if err != nil {
			return nil, err
		}

		var dest map[string]interface{}
		err = json.Unmarshal(b, &dest)

		return &JObject{dest}, err
	}

	return nil, err(file, "file doesn't exist")
}

func LoadJObjectMap(file string) (*JObject, error) {
	if _, err := os.Stat(file); err == nil {
		b, err := ioutil.ReadFile(file)
		if err != nil {
			return nil, err
		}

		var dest map[string]interface{}
		err = json.Unmarshal(b, &dest)

		return &JObject{dest}, err
	}

	mp := make(map[string]interface{})

	return &JObject{mp}, nil
}

func err(file, msg string) error {
	return errors.New("Failed to load " + file + ": " + msg)
}
